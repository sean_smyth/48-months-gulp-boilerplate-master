var gulp = require("gulp");
var sass = require("gulp-sass");
var prefix = require("gulp-autoprefixer");
var browserSync = require("browser-sync").create();

var prefixerOptions = {
  browsers: ["last 2 versions"]
};

gulp.task("sass", function() {
  return gulp
    .src("./sass/ss_styles.scss")
    .pipe(sass())
    .pipe(prefix(prefixerOptions))
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./mobile/index/"))
    .pipe(browserSync.stream());
});

gulp.task("serve", ["sass"], function() {
  browserSync.init({
    server: ""
  });

  gulp.watch("./sass/*.scss", ["sass"]);
  gulp.watch("./*.html").on("change", browserSync.reload);
});

gulp.task("default", ["serve"]);
